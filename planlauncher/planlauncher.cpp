/***************************************************************************
 *   Copyright (C) 2010 by Sandro Andrade <sandroandrade@kde.org>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include <QHash>
#include <QFile>
#include <QtDebug>
#include <QVariant>
#include <QDomDocument>
#include <QPluginLoader>
#include <QSharedPointer>
#include <QCoreApplication>
#include <QXmlSchema>
#include <QXmlSchemaValidator>

#include <qcm/qcmobjecthome.h>

class PlanLauncher
{
public:
    PlanLauncher();
    virtual ~PlanLauncher();
    int exec(const QString &fileName);
private:
    void loadHome(const QString &id, const QString &libraryName);
    QSharedPointer<QObject> createInstance(const QString &homeId, const QString &instanceId, const QString &instanceName);
    void createConnection(const QString &name, const QString &firstEndPoint, const QString &firstPortName, const QString &secondEndPoint, const QString &secondPortName);
    
    QHash<QString, QCMIObjectHome *> m_factories;
    QHash<QString, QSharedPointer<QObject> > m_instances;
};

PlanLauncher::PlanLauncher()
{
}

PlanLauncher::~PlanLauncher()
{    
}

int PlanLauncher::exec(const QString &fileName)
{
    QFile schemaFile("qcm-deployment.xsd");

    if (!schemaFile.open(QFile::ReadOnly | QFile::Text))
    {
        qCritical() << QObject::tr("Cannot read schema file qcm-deployment.xsd: %2.").arg(schemaFile.errorString());
        return 1;
    }

    QXmlSchema schema;
    if (!schema.load(&schemaFile))
    {
        qCritical() << QObject::tr("Invalid schema qcm-deployment.xsd.");
        return 1;
    }

    QFile file(fileName);

    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qCritical() << QObject::tr("Cannot read file %1: %2.").arg(fileName).arg(file.errorString());
        return 1;
    }

    QXmlSchemaValidator validator(schema);
    if (!validator.validate(&file))
    {
        qCritical() << QObject::tr("Invalid QCM deployment descriptor %1.").arg(fileName);
        return 1;
    }

    file.reset();
    QDomDocument domDocument;

    QString errorStr;
    int errorLine;
    int errorColumn;
    
    if (!domDocument.setContent(&file, true, &errorStr, &errorLine, &errorColumn))
    {
        qCritical() << QObject::tr("Parse error at line %1, column %2: %3").arg(errorLine).arg(errorColumn).arg(errorStr);
        return 1;
    }

    QDomElement root = domDocument.documentElement();

    QDomElement implementation = root.firstChildElement("implementation");
    while (!implementation.isNull())
    {
        loadHome(implementation.attribute("id"), implementation.firstChildElement("library").text());
        implementation = implementation.nextSiblingElement("implementation");
    }

    QDomElement instance = root.firstChildElement("instance");
    while (!instance.isNull())
    {
        QString instanceName = instance.firstChildElement("name").text();
        QSharedPointer<QObject> instanceObj = createInstance(instance.firstChildElement("implementation").attribute("idref"),
                                                             instance.attribute("id"), instanceName);
        QDomElement property = instance.firstChildElement("configProperty");
        while (!property.isNull())
        {
            QString propertyName = property.firstChildElement("name").text();
            if (!instanceObj->setProperty(propertyName.toLatin1(),
                                          QVariant(property.firstChildElement("value").firstChildElement("value").text())))
            {
                instanceObj->setProperty(propertyName.toLatin1(), QVariant());
                qDebug() << "ERROR: instance " << instance.firstChildElement("name").text() << "doesn't contain property " << propertyName;
            }
            property = property.nextSiblingElement("configProperty");
        }
        instance = instance.nextSiblingElement("instance");
    }

    QDomElement connection = root.firstChildElement("connection");
    while (!connection.isNull())
    {
        QDomElement firstEndPoint = connection.firstChildElement("internalEndpoint");
        QDomElement secondEndPoint = firstEndPoint.nextSiblingElement("internalEndpoint");
        createConnection(connection.firstChildElement("name").text(),
                         firstEndPoint.firstChildElement("instance").attribute("idref"),
                         firstEndPoint.firstChildElement("portName").text(),
                         secondEndPoint.firstChildElement("instance").attribute("idref"),
                         secondEndPoint.firstChildElement("portName").text());
        connection = connection.nextSiblingElement("connection");
    }

    QMetaObject::invokeMethod(m_instances["SenderComponentInstance"].data(), "go");
    return 0;
}

void PlanLauncher::loadHome(const QString &id, const QString &libraryName)
{
    QString qcmInstall = QString(getenv("QCM_INSTALL"));	// krazy:exclude=syscalls
    if (qcmInstall.isEmpty())
        qcmInstall = "/usr";
    qDebug() << "Loading " << qcmInstall + "/lib/lib" + libraryName + ".so";
    QPluginLoader loader(qcmInstall + "/lib/lib" + libraryName + ".so");

    // We use dynamic_cast instead of qobject_cast because the latter requires a Q_OBJECT
    // and Qt doesn't support the joint use of templates and Q_OBJECT macro
    QCMIObjectHome *home = dynamic_cast<QCMIObjectHome *>(loader.instance());

    if (home)
        m_factories[id] = home;
    else
        qCritical() << QObject::tr("Cannot load home %1. Error: %2").arg(id).arg(loader.errorString());
}

QSharedPointer<QObject> PlanLauncher::createInstance(const QString &homeId, const QString &instanceId, const QString &instanceName)
{
    QCMIObjectHome *home = m_factories[homeId];
    if (home)
    {
        QSharedPointer<QObject> instance (home->createInstance());
        instance->setObjectName(instanceName);
        m_instances[instanceId] = instance;
        return instance;
    }
    qCritical() << QObject::tr("Cannot create instance [homeId=%1, id=%2, name=%3").arg(homeId).arg(instanceId).arg(instanceName);
    return QSharedPointer<QObject>();
}

void PlanLauncher::createConnection(const QString &name, const QString &firstEndPoint, const QString &firstPortName, const QString &secondEndPoint, const QString &secondPortName)
{
    Q_UNUSED(name);
    
    QSharedPointer<QObject> firstInstance = m_instances[firstEndPoint];
    QSharedPointer<QObject> secondInstance = m_instances[secondEndPoint];
    
    if (firstInstance.isNull())
    {
        qCritical() << QObject::tr("Cannot find endpoint %1.").arg(firstEndPoint);
        return;
    }
    if (secondInstance.isNull())
    {
        qCritical() << QObject::tr("Cannot find endpoint %1.").arg(secondEndPoint);
        return;
    }

    QByteArray theSignal = QMetaObject::normalizedSignature(qPrintable(firstPortName));
    QByteArray theSlot = QMetaObject::normalizedSignature(qPrintable(secondPortName));

    if (firstInstance->metaObject()->indexOfSignal(theSignal) == -1)
    {
        qCritical() << QObject::tr("Cannot find signal %1.").arg(firstPortName);
        return;
    }
    if (secondInstance->metaObject()->indexOfSlot(theSlot) == -1)
    {
        qCritical() << QObject::tr("Cannot find slot %1.").arg(secondPortName);
        return;
    }
    
    if (!QMetaObject::checkConnectArgs(theSignal, theSlot))
    {
        qCritical() << QObject::tr("Signal and slot do not match !");
        return;
    }

    QMetaObject::connect(firstInstance.data(), firstInstance->metaObject()->indexOfSignal(theSignal),
                         secondInstance.data(), secondInstance->metaObject()->indexOfSlot(theSlot));
}

int main (int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    if (argc != 2)
    {
        qCritical() << QObject::tr("Usage: planlauncher <deployment-descriptor.qdd>");
        return 1;
    }
    PlanLauncher planLauncher;
    return planLauncher.exec(argv[1]);
}
